function isNumber(e) {
    var ASCIICode = e.which ? e.which : e.keyCode;
    if (
        ASCIICode === 8 ||
        ASCIICode === 46 ||
        ASCIICode === 37 ||
        ASCIICode === 9 ||
        ASCIICode === 116 ||
        ASCIICode === 39
    ) {
        return true;
    }
    if (ASCIICode < 48 || ASCIICode > 57) return e.preventDefault();
    return true;
}

if (document.getElementsByClassName("only-numbers").length > 0) {
    document.querySelectorAll(".only-numbers").forEach((item) => {
        item.addEventListener("keydown", (event) => {
            return isNumber(event);
        });
    });
}
