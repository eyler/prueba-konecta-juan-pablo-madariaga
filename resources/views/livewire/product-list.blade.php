{{-- Aquí definimos el nombre de la pesataña en el navegador --}}
<x-slot name="title">Lista de productos</x-slot>
<div>
    {{-- Aquí definimos el contenido del modulo. --}}
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Lista de productos</h4>
        </div>
        <div class="card-body">

            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 mb-2 text-right">
                        <a class="btn btn-primary" href="{{ route('new-product') }}" role="button">Nuevo producto</a>
                    </div>
                    <div class="col-12">
                        {{-- Validamos que si existan productos --}}
                        @if (!$products->isEmpty())
                        <div class="table-responsive">
                            <table class="table table-striped table-inverse mb-0" style="min-width: 1050px">
                                <thead class="thead-inverse">
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Referencia</th>
                                        <th>Precio</th>
                                        <th>Peso</th>
                                        <th>Stock</th>
                                        <th>Categoría</th>
                                        <th>Ultima venta</th>
                                        <th>Fecha de creación</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {{-- Recorremos la lista de los productos recibida desde el controlador de livewire --}}
                                    @foreach ($products as $product)
                                    <tr>
                                        <td scope="row">{{$product->product_name}}</td>
                                        <td>{{$product->reference}}</td>
                                        <td>{{$product->price }}</td>
                                        <td>{{$product->weight}}</td>
                                        <td>{{$product->stock}}</td>
                                        <td>{{$product->category}}</td>
                                        <td>{{$product->last_sale == null ? 'No' : $product->last_sale}}</td>
                                        <td>{{$product->created_at}}</td>
                                        <td style="min-width: 260px">
                                            <div class="flex">
                                                <a type="button" class="btn btn-primary btn-sm"
                                                    href="{{ route('product', ['id'=>$product->id]) }}">Ver</a>
                                                <a type="button" class="btn btn-secondary  btn-sm"
                                                    href="{{ route('edit-product', ['id'=>$product->id]) }}">Editar</a>
                                                <button type="button" wire:click="sell({{$product->id}})"
                                                    class="btn btn-success  btn-sm">Vender</button>
                                                <button type="button" wire:click="delete({{$product->id}})"
                                                    class="btn btn-danger  btn-sm">Borrar</button>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{-- Definimos vista de paginación que provee livewire --}}
                        {{ $products->links() }}


                        @else
                        {{-- Si no existen productos se muestra el mensaje --}}
                        <h6 class="text-center">No existen produtos para mostrar</h6>
                        @endif
                        <div class="alert alert-success alert-dismissible mt-4" :class="{'fade show':show}" role="alert"
                            x-data="{show:false}" x-init="@this.on('success', () => {show=true})" x-show="show">
                            <button type="button" class="close" aria-label="Close" @click="show=false">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <strong>{{$this->msg}}</strong>
                        </div>

                        <div class="alert alert-danger alert-dismissible mt-4" :class="{'fade show':show}" role="alert"
                            x-data="{show:false}" x-init="@this.on('warning', () => {show=true})" x-show="show">
                            <button type="button" class="close" aria-label="Close" @click="show=false">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <strong>{{$this->msg}}</strong>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
