{{-- Aquí definimos el nombre de la pesataña en el navegador --}}
<x-slot name="title">Editar producto</x-slot>
<div>
    {{-- Aquí definimos el contenido del modulo. --}}
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Editar producto</h4>
        </div>
        <div class="card-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 mb-2 text-right">
                        <a class="btn btn-primary" href="{{ route('products') }}" role="button">Productos</a>
                    </div>
                    <div class="col-12">

                        <div class="form-row">
                            <div class="col-12 col-sm-6 col-lg-4 mt-3">
                                <input type="text" class="form-control disabled" readonly wire:model="product_name"
                                    maxlength="100" placeholder="Nombre del producto">
                            </div>
                            <div class="col-12 col-sm-6 col-lg-4 mt-3">
                                <input type="text" class="form-control disabled" readonly wire:model="reference"
                                    maxlength="190" placeholder="Referencia">
                            </div>
                            <div class="col-12 col-sm-6 col-lg-4 mt-3">
                                <input type="text" class="form-control only-numbers disabled" readonly
                                    wire:model="price" maxlength="9" placeholder="Precio">
                            </div>
                            <div class="col-12 col-sm-6 col-lg-4 mt-3">
                                <input type="text" class="form-control only-numbers disabled" readonly
                                    wire:model="weight" maxlength="9" placeholder="Peso">
                            </div>
                            <div class="col-12 col-sm-6 col-lg-4 mt-3">
                                <input type="text" class="form-control only-numbers disabled" readonly
                                    wire:model="stock" maxlength="8" placeholder="Cantidad inicial">
                            </div>
                            <div class="col-12 col-sm-6 col-lg-4 mt-3">
                                <input type="text" class="form-control disabled" readonly wire:model="category"
                                    maxlength="80" placeholder="Categoría">
                            </div>
                            <div class="col-12 mt-2">
                                <div class="row">

                                    <div class="col-md-6 col-12 mt-2 text-right">
                                        <a href="{{ route('edit-product', ['id'=>$product->id]) }}"
                                            class="btn btn-secondary" type="button">Editar producto</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
