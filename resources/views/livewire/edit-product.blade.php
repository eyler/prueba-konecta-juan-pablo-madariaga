{{-- Aquí definimos el nombre de la pesataña en el navegador --}}
<x-slot name="title">Editar producto</x-slot>
<div>
    {{-- Aquí definimos el contenido del modulo. --}}
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Editar producto</h4>
        </div>
        <div class="card-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 mb-2 text-right">
                        <a class="btn btn-primary" href="{{ route('products') }}" role="button">Productos</a>
                    </div>
                    <div class="col-12">
                        <form wire:submit.prevent="updateProduct">
                            <div class="form-row">
                                <div class="col-12 col-sm-6 col-lg-4 mt-3">
                                    <input type="text" class="form-control @error('product_name') is-invalid @enderror"
                                        wire:model="product_name" maxlength="100" placeholder="Nombre del producto">
                                    @error('product_name') <span class="invalid-feedback">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="col-12 col-sm-6 col-lg-4 mt-3">
                                    <input type="text" class="form-control @error('reference') is-invalid @enderror"
                                        wire:model="reference" maxlength="190" placeholder="Referencia">
                                    @error('reference') <span class="invalid-feedback">{{ $message }}</span> @enderror
                                </div>
                                <div class="col-12 col-sm-6 col-lg-4 mt-3">
                                    <input type="text"
                                        class="form-control only-numbers @error('price') is-invalid @enderror"
                                        wire:model="price" maxlength="9" placeholder="Precio">
                                    @error('price') <span class="invalid-feedback">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 col-sm-6 col-lg-4 mt-3">
                                    <input type="text"
                                        class="form-control only-numbers @error('weight') is-invalid @enderror"
                                        wire:model="weight" maxlength="9" placeholder="Peso">
                                    @error('weight') <span class="invalid-feedback">{{ $message }}</span> @enderror
                                </div>
                                <div class="col-12 col-sm-6 col-lg-4 mt-3">
                                    <input type="text"
                                        class="form-control only-numbers @error('stock') is-invalid @enderror"
                                        wire:model="stock" maxlength="8" placeholder="Cantidad inicial">
                                    @error('stock') <span class="invalid-feedback">{{ $message }}</span> @enderror
                                </div>
                                <div class="col-12 col-sm-6 col-lg-4 mt-3">
                                    <input type="text" class="form-control @error('category') is-invalid @enderror"
                                        wire:model="category" maxlength="80" placeholder="Categoría">
                                    @error('category') <span class="invalid-feedback">{{ $message }}</span> @enderror
                                </div>
                                <div class="col-12 mt-2">
                                    <div class="row">
                                        <div class="col-md-6 col-12 mt-2">
                                            <div class="alert alert-success alert-dismissible"
                                                :class="{'fade show':show}" role="alert" x-data="{show:false}"
                                                x-init="@this.on('updated', () => {show=true})" x-show="show">
                                                <button type="button" class="close" aria-label="Close"
                                                    @click="show=false">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                <strong>Actualizado con éxito.</strong>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12 mt-2 text-right">
                                            <button class="btn btn-success" type="submit">Actualizar producto</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
