<?php

namespace App\Http\Livewire;

use App\Models\Product;
use Livewire\Component;
use Livewire\WithPagination;

class ProductList extends Component
{
    //Usamos la paginación de livewire
    use WithPagination;

    public $msg;

    //Le decimos a la paginación que utilice bootstrap
    protected $paginationTheme = 'bootstrap';


    public function sell($id)
    {
        $product = Product::find($id);
        if ($product != null) {
            //Si el producto tiene stock, realizamos la venta y actualizamos el producto
            if ($product->stock > 0) {
                $product->stock = $product->stock - 1;
                $product->last_sale = date('Y-m-d H:i:s');
                $product->save();
                //emitimos el evento success si existe el producto y se vende
                $this->msg = 'Producto vendido.';
                $this->emit('success');
            } else {
                // De lo contrario emitimos el evento error si no hay stock
                $this->msg = 'No hay stock para vender este producto.';
                $this->emit('warning');
            }
        } else {
            //emitimos el evento error si no existe el producto a vender
            $this->msg = 'Producto no existe.';
            $this->emit('warning');
        }
    }

    public function delete($id)
    {
        $product = Product::find($id);
        if ($product != null) {
            $product->delete();
            //emitimos el evento success si existe el producto y se elimina
            $this->msg = 'Producto eliminado.';
            //refrescamos la paginación
            $this->resetPage();
            $this->emit('success');
        } else {
            //emitimos el evento error si no existe el producto
            $this->msg = 'Producto no existe.';
            $this->emit('warning');
        }
    }

    public function render()
    {
        return view('livewire.product-list', [
            //Enviamos todos los productos paginando por 10 a la vista de lista de productos
            'products' => Product::paginate(8),
        ]);
    }
}
