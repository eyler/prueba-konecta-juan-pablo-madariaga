<?php

namespace App\Http\Livewire;

//Usamos el modelo de producto
use App\Models\Product;
use Livewire\Component;

class EditProduct extends Component
{

    //Variables a utilizar en la vista de liveWire New Product
    public $product_name, $reference, $price, $weight, $stock, $category;
    public $product, $rl;


    public function mount($id)
    {
        $id;
        //traemos el producto a actualizar
        $product = Product::find($id);
        //validamos si el producto no existe
        if ($product == null) {
            //redirigimos a crear un nuevo producto
            return redirect()->to('/new-product');
        } else {
            //si existe lo definimos globalmente para empezar a modificar
            $this->product = $product;
            //definimos los campos en base al producto
            $this->product_name = $this->product->product_name;
            $this->reference = $this->product->reference;
            $this->price = $this->product->price;
            $this->weight = $this->product->weight;
            $this->stock = $this->product->stock;
            $this->category = $this->product->category;
            //Reglas para validar el formulario
            $this->rl = [
                'product_name' => "required|min:2|max:100|unique:products,product_name," . $this->product->id,
                'reference' => "required|min:2|max:190|unique:products,reference," . $this->product->id,
                'price' => 'required|integer|digits_between:2,10',
                'weight' => 'required|integer|digits_between:1,10',
                'stock' => 'required|integer|digits_between:0,8',
                'category' => 'required|max:80',
            ];
        }
    }

    /**
     * Se ejecuta cada vez que hay un evento dentro de los campos de la vista
     * @var string $fieldName
     */
    public function updated($fieldName)
    {
        //validamos cada campo individualmente
        $this->validateOnly($fieldName, $this->rl);
    }

    /**
     * Función para actualizar el producto en la base de datos
     */
    public function updateProduct()
    {
        //Realizamos la validación de los datos recibidos antes de guardar el producto
        $this->validate($this->rl);
        //definimos los datos del nuevo producto tomados de las variables del componente livewire
        $this->product->product_name = $this->product_name;
        $this->product->reference = $this->reference;
        $this->product->price = $this->price;
        $this->product->weight = $this->weight;
        $this->product->stock = $this->stock;
        $this->product->category = $this->category;
        //Guardamos el producto en la base de datos gracias al ORM de laravel (Eloquent)
        $this->product->save();

        //emitimos el evento updated si se actualiza
        $this->emit('updated');
    }

    public function render()
    {
        return view('livewire.edit-product');
    }
}
