<?php

namespace App\Http\Livewire;

//Usamos el modelo de producto
use App\Models\Product as MProduct;
use Livewire\Component;

class Product extends Component
{

    //Variables a utilizar en la vista de liveWire New Product
    public $product_name, $reference, $price, $weight, $stock, $category;
    public $product, $rl;


    public function mount($id)
    {
        $id;
        //traemos el producto a actualizar
        $product = MProduct::find($id);
        //validamos si el producto no existe
        if ($product == null) {
            //redirigimos a crear un nuevo producto
            return redirect()->to('/new-product');
        } else {
            //si existe lo definimos globalmente para empezar a modificar
            $this->product = $product;
            //definimos los campos en base al producto
            $this->product_name = $this->product->product_name;
            $this->reference = $this->product->reference;
            $this->price = $this->product->price;
            $this->weight = $this->product->weight;
            $this->stock = $this->product->stock;
            $this->category = $this->product->category;
        }
    }

    public function render()
    {
        return view('livewire.product');
    }
}
