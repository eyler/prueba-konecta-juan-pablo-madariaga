<?php

namespace App\Http\Livewire;

//Usamos el modelo de producto
use App\Models\Product;
use Livewire\Component;

class NewProduct extends Component
{

    //Variables a utilizar en la vista de liveWire New Product
    public $product_name, $reference, $price, $weight, $stock, $category;


    //Reglas para validar el formulario
    protected $rules = [
        'product_name' => 'required|min:2|max:100|unique:products',
        'reference' => 'required|min:2|max:190|unique:products',
        'price' => 'required|integer|digits_between:2,10',
        'weight' => 'required|integer|digits_between:1,10',
        'stock' => 'required|integer|digits_between:0,8',
        'category' => 'required|max:80',
    ];

    /**
     * Se ejecuta cada vez que hay un evento dentro de los campos de la vista
     * @var string $fieldName
     */
    public function updated($fieldName)
    {
        //validamos cada campo individualmente
        $this->validateOnly($fieldName);
    }

    /**
     * Función para crear un nuevo producto en la base de datos
     */
    public function createProduct()
    {
        //Realizamos la validación de los datos recibidos antes de guardar el producto
        $this->validate();
        // definimos el nuevo producto en base al modelo producto
        $producto = new Product();
        //definimos los datos del nuevo producto tomados de las variables del componente livewire
        $producto->product_name = $this->product_name;
        $producto->reference = $this->reference;
        $producto->price = $this->price;
        $producto->weight = $this->weight;
        $producto->stock = $this->stock;
        $producto->category = $this->category;
        //Guardamos el producto en la base de datos gracias al ORM de laravel (Eloquent)
        $producto->save();

        //Limpiamos los campos del form
        $this->product_name = '';
        $this->reference = '';
        $this->price = '';
        $this->weight = '';
        $this->stock = '';
        $this->category = '';
        $this->emit('created');
    }

    public function render()
    {
        return view('livewire.new-product');
    }
}
