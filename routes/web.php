<?php

use App\Http\Livewire\EditProduct;
use App\Http\Livewire\NewProduct;
use App\Http\Livewire\Product;
use App\Http\Livewire\ProductList;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Definimos la ruta de la lista de productos cómo inicial
Route::get('/', ProductList::class)->name('products');
//Definimos la ruta para crear un nuevo producto
Route::get('/new-product', NewProduct::class)->name('new-product');
//Definimos la ruta para ver un producto
Route::get('/product/{id}', Product::class)->name('product');
Route::get('/edit-product/{id}', EditProduct::class)->name('edit-product');
